#include "simulator.hpp"
#include "processor.hpp"
#include <iostream>


// =====================================================================
processor_t::processor_t() {
};

// =====================================================================
void processor_t::allocate() {
    opcode_package_t new_instruction;
    orcs_engine.trace_reader->trace_fetch(&new_instruction);
    this->lastOp = new_instruction;
};

// =====================================================================
void processor_t::clock() {

    /// Get the next instruction from the trace
    opcode_package_t new_instruction;
    if (!orcs_engine.trace_reader->trace_fetch(&new_instruction)) {
        /// If EOF
        orcs_engine.simulator_alive = false;
    } else {
        if(new_instruction.opcode_operation == INSTRUCTION_OPERATION_BRANCH){
            this->btb.push(new_instruction.opcode_address, true);
            this->btb.print();
        }
    }

};

// =====================================================================
void processor_t::statistics() {
    ORCS_PRINTF("######################################################\n");
    ORCS_PRINTF("processor_t\n");

};


BTB::BTB() {
}

void BTB::push(uint64_t opcodeAddress, bool taken = true) {
    uint64_t index = opcodeAddress & 0x1111111111;
    uint64_t tag = opcodeAddress >> 10;

    std::bitset<16> y(opcodeAddress);
    std::bitset<16> x(index);
    std::bitset<16> z(tag);
    std::cout << y << " " << x << " " << z;

    BTB_Row *foundRow = NULL;
    for (uint8_t i = 0; i < 4; i++) {
        if (this->tables[i][index].tag) {
            foundRow = &this->tables[i][index];
            break;
        }
    }
    if (foundRow == NULL) {
        foundRow = &this->tables[0][index];
        for (uint8_t i = 0; i < 4; i++) {
            if (this->tables[i][index].cycle < foundRow->cycle) {
                foundRow = &this->tables[i][index];
            }
        }
    }
    foundRow->cycle = orcs_engine.global_cycle;
    foundRow->taken = taken;
    foundRow->tag = tag;
}

BTB_Row *BTB::get(uint64_t opcodeAddress) {
    uint64_t index = opcodeAddress & 0x1111111111;
    uint64_t tag = opcodeAddress >> 10;
    BTB_Row *foundRow = NULL;
    for (uint8_t i = 0; i < 4; i++) {
        if (this->tables[i][index].tag == tag) {
            foundRow = &this->tables[i][index];
            break;
        }
    }
    return foundRow;
}


void BTB::print() {
    for (int i = 0; i < 50; i++) {
//        for(int j = 0; j < 4; j++){
//            std::cout << this->tables[0][i].tag << "|";
//        }

    }
    std::cout << "\n";
}

