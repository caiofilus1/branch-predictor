// ============================================================================
// ============================================================================

#ifndef PROCESSOR
#define PROCESSOR
#include "simulator.hpp"

struct BTB_Row {
    uint64_t tag;
    uint64_t cycle;
    bool taken;
};

class BTB {
    private:
        BTB_Row tables[4][1024];
    public:
        BTB();
        BTB_Row* get(uint64_t opcodeAddress);
        void push(uint64_t opcodeAddress, bool taken);
        void print();
};

class processor_t {
    private:
    BTB btb;
    opcode_package_t lastOp;
    public:

		// ====================================================================
		/// Methods
		// ====================================================================
		processor_t();
	    void allocate();
	    void clock();
	    void statistics();
};
#endif